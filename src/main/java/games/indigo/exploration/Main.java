package games.indigo.exploration;

import co.aikar.commands.PaperCommandManager;
import games.indigo.exploration.commands.ExplorationCommand;
import games.indigo.exploration.listeners.RegionEnteredListener;
import me.themgrf.coinsapi.CoinsAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private CoinsAPI coins;

    private static Main instance;

    private AreaManager areaManager;
    private FileManager fileManager;

    public void onEnable() {
        instance = this;

        coins = getServer().getServicesManager().getRegistration(CoinsAPI.class).getProvider();

        fileManager = new FileManager(this);
        areaManager = new AreaManager();
        areaManager.load();
        //fileManager.getConfig("areas.yml").get().getKeys(false)

        // Commands
        PaperCommandManager manager = new PaperCommandManager(this);
        manager.registerCommand(new ExplorationCommand());

        // Listeners
        Bukkit.getPluginManager().registerEvents(new RegionEnteredListener(), this);
    }

    public static Main getInstance() {
        return instance;
    }

    public CoinsAPI getCoins() {
        return coins;
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public AreaManager getAreaManager() {
        return areaManager;
    }
}
