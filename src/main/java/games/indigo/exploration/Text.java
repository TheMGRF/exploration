package games.indigo.exploration;

import org.bukkit.ChatColor;

import java.util.List;

public class Text {

    /**
     * Format a string to contain Bukkit colour codes
     *
     * @param msg The message to format
     * @return The formatted colour coded string
     */
    public static String colour(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    /**
     * Format an array to contain Bukkit colour codes
     *
     * @param lore The array to format
     * @return The formatted colour coded array
     */
    public static List<String> colourArray(List<String> lore) {
        for (int x = 0; x < lore.size(); x++) {
            lore.set(x, colour(lore.get(x)));
        }
        return lore;
    }

}
