package games.indigo.exploration.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.*;
import games.indigo.exploration.*;
import org.bukkit.entity.Player;

@CommandAlias("explore|exploration")
@CommandPermission("indigo.exploration.admin")
public class ExplorationCommand extends BaseCommand {

    private FileManager.Config config = Main.getInstance().getFileManager().getConfig("areas.yml");
    private AreaManager areaManager = Main.getInstance().getAreaManager();

    @Default
    public void exploration(Player player) {
        //./exploration add <region> <name> <coins>
        player.sendMessage("Help");
    }

    @Subcommand("add")
    @Syntax("<region id> <region name> <coins> &e- Add an exploration region")
    public void addArea(Player player, String region, String name, int coins) {
        config.set("areas." + region + ".name", name);
        config.set("areas." + region + ".coins", coins);
        config.save();

        areaManager.addArea(new Area(region, name, coins));
        player.sendMessage("created area with region " + region + " named " + name + " &fearning the finder " + coins + " coins!");
    }

    @Subcommand("remove")
    @Syntax("<region id> &e- Remove an exploration region")
    public void removeArea(Player player, String region) {
        config.set("areas." + region, null);
        config.save();

        areaManager.removeArea(areaManager.getAreaByName(region));

        player.sendMessage("removed area with region name: " + region);
    }

    @Subcommand("reload")
    public void reload(Player player) {
        player.sendMessage(Text.colour("&aReloaded areas config!"));
        config.reload();
    }

    @Subcommand("list")
    public void listAreas(Player player) {
        player.sendMessage("Areas:");
        areaManager.getAreas().forEach(area -> player.sendMessage(area.getRegion() + ": " + area.getName() + " &7- &e" + area.getCoins() + " Coins"));
    }
}
