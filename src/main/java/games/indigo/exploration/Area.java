package games.indigo.exploration;

public class Area {

    private String region, name;
    private int coins;

    /**
     * Constructor for creating an area
     * @param region The world guard region associated with the area
     * @param name The display name of the world
     * @param coins The amount of coins earned for finding the area
     */
    public Area(String region, String name, int coins) {
        this.region = region;
        this.name = name;
        this.coins = coins;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
