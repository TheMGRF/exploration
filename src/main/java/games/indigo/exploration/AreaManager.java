package games.indigo.exploration;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class AreaManager {

    private Main instance = Main.getInstance();

    private FileManager.Config config = instance.getFileManager().getConfig("areas.yml");

    private List<Area> areas;

    public void load() {
        if (config.get().get("areas") == null) config.saveDefaultConfig();
        areas = loadAreasFromConfig();
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void addArea(Area area) {
        areas.add(area);
    }

    public void removeArea(Area area) {
        areas.remove(area);
    }

    public Area getAreaByName(String name) {
        for (Area area : getAreas()) {
            if (area.getRegion().equals(name)) return area;
        }

        return null;
    }

    public void foundArea(Player player, Area area) {
        player.sendTitle(Text.colour(area.getName() + " &f&lDiscovered!"), Text.colour("&e+" + area.getCoins() + " Coins"), 10, 30, 10);
        List<String> uuids = getWhoDiscoverd(area);
        uuids.add(player.getUniqueId().toString());

        instance.getCoins().addCoins(player.getName(), area.getCoins());

        config.set("areas." + area.getRegion() + ".discovered", uuids);
        config.save();
    }

    public boolean hasFoundArea(Player player, Area area) {
        List<String> uuids = getWhoDiscoverd(area);
        for (String uuid : uuids) {
            if (player.getUniqueId().toString().equals(uuid)) {
                return true;
            }
        }
        return false;
    }

    public List<String> getWhoDiscoverd(Area area) {
        return config.get().getStringList("areas." + area.getRegion() + ".discovered");
    }

    private List<Area> loadAreasFromConfig() {
        List<Area> areas = new ArrayList<>();
        for (String areaRegion : config.get().getConfigurationSection("areas").getKeys(false)) {
            String name = config.get().getString("areas." + areaRegion + ".name");
            int coins = config.get().getInt("areas." + areaRegion + ".coins");
            areas.add(new Area(areaRegion, name, coins));
        }

        return areas;
    }

}
