package games.indigo.exploration.listeners;

import games.indigo.exploration.Area;
import games.indigo.exploration.Main;
import games.indigo.exploration.Text;
import net.raidstone.wgevents.events.RegionEnteredEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class RegionEnteredListener implements Listener {

    private Main instance = Main.getInstance();

    @EventHandler
    public void onRegionEnter(RegionEnteredEvent e) {
        if (isPlayer(e.getPlayer())) {
            Player player = e.getPlayer();

            for (Area area : instance.getAreaManager().getAreas()) {
                if (area.getRegion().equalsIgnoreCase(e.getRegionName())) {
                    if (instance.getAreaManager().hasFoundArea(player, area)) {
                        player.sendActionBar(Text.colour("&f&lEntering " + area.getName()));
                    } else {
                        instance.getAreaManager().foundArea(e.getPlayer(), area);
                    }
                    return;
                }
            }
        }
    }

    public boolean isPlayer(Player player) {
        return Bukkit.getOnlinePlayers().contains(player);
    }

}
